#! /usr/bin/php
<?php

if ((2 === $argc) && (('-h' === $argv[1]) || ('--help' === $argv[1]))) {
    echo "First time, you might want to launch it with a dummy argument, like \"love-dialog.php whamo\" or something. That will generate a new session key for you. In the process, you will have to grant permission to this application (with my key it's \"playingaround\") by visiting a URL provided by this application.\n";
    die("Otherwise, until it starts to fail, just call it with no argument, like \"love-dialog.php\".\n");
}

// TODO: make failures more obvious, give user more feedback.

require 'love-playing.inc.php';

list($cover, $artist, $title, $dialog) = initLoveDialog($user, true); // false (default): unextended (faster, no 2nd call to API)
launchLoveDialog($dialog, $artist, $title);
unlink($cover);

function getSongInfo($user, $extended) {
    $playing = getNowPlaying($user, $extended);
    $title   = (string)$playing->name;
    $artist  = (string)$playing->artist;
    $album   = (string)$playing->album;
    $elapsed = (int)$playing->more->elapsed;
    if (abs($elapsed - time()) < 5) $elapsed = 0;
    $plays   = (int)$playing->more->play_count;
    $loved   = (string)$playing->more->loved;
    $mbid    = $playing->mbid;
    $cover   = tempnam(sys_get_temp_dir(), 'lfm');
    @copy((string)$playing->image[2], $cover);
    return array($title, $artist, $album, $cover, $elapsed, $mbid, $plays, $loved);
}

function initLoveDialog($user, $extended = false) {
    list($title, $artist, $album, $cover, $elapsed, $mbid, $plays, $loved) = getSongInfo($user, $extended);
    return array($cover, $artist, $title, makeLoveDialog($title, $artist, $album, $cover, $elapsed, $mbid, $plays, $loved));
}

function launchDialog($dialog) {
    putenv("MAIN_DIALOG=$dialog");
    $options = array();
    $fp      = popen('gtkdialog --center --program=MAIN_DIALOG', 'r');
    if (!$fp) return false;
    while (!feof($fp)) {
        @list($k, $v) = explode('=', fgets($fp, 255));
        if (isset($v)) $options[$k] = trim($v, "\"\n");
    }
    if (fclose($fp)) return $options;
    return false;
}

function launchLoveDialog($dialog, $artist, $title) {
    $options = launchDialog($dialog);
    if (false === $options) return false;
    if ('gtk-ok' === $options['EXIT']) {
        $sk = getSessionKey(); 
        return loveTrack($artist, $title, $sk);
    }
    return false;
}

function makeLoveDialog($title, $artist, $album, $cover, $elapsed, $mbid, $plays, $loved) {
    $elapsed_str = $elapsed
        ? ('<hbox><text><label>Elapsed: ' . gmdate('H:i:s', $elapsed) . ' ago</label></text></hbox>')
        : '';
    $plays_hbox  = $plays ? "<hbox><text><label>Plays: $plays</label></text></hbox>" : '';
    $loved_hbox  = $loved ? "<hbox><text><label>Already loved :)</label></text></hbox>" : '';
    $album = $album
        ? "<hbox><text><label>Album: $album</label></text></hbox>"
        : '';

    return <<<E_O_T
<vbox>
<frame Confirm you love this song>
<hbox>
    <pixmap>
        <input file>$cover</input>
    </pixmap>
    <vbox>
        <hbox><text><label>Title: $title</label></text></hbox>
        <hbox><text><label>Artist: $artist</label></text></hbox>
        $album
        $elapsed_str
        $plays_hbox
        $loved_hbox
    </vbox>
</hbox>
</frame>
<hbox>
    <button can-default="true" has-default="true" use-stock="true">
        <label>gtk-ok</label>
    </button>
    <button use-stock="true">
        <label>gtk-cancel</label>
    </button>
</hbox>
</vbox>
E_O_T;
}

// might ask the user to visit website to confirm access
function getSessionKey() {
    global $argc;
    $session_filename = 'lastfm-love.session';

    if ($argc == 1) {
        $ar = parse_ini_file($session_filename);
        if (isset($ar['session_key'])) return $ar['session_key'];
    }

    global $api_root;
    global $api_key;

    $xml    = new SimpleXMLElement("$api_root?method=auth.gettoken&api_key=$api_key", null, true);
    $token  = (string)$xml->token;
    $dialog =<<<E_O_T
<vbox>
<frame Grant permission to playingaround last.fm application>
<vbox>
<text><label>Visit</label></text>
<text selectable="true"><label>http://www.last.fm/api/auth?api_key=$api_key&token=$token</label></text>
<text><label>(login if necessary) and allow this program (called 'playingaround' on last.fm) to continue. I will wait until you press a key...</label></text>
</vbox>
</frame>
<hbox>
    <button can-default="true" has-default="true" use-stock="true">
        <label>gtk-ok</label>
    </button>
</hbox>
</vbox>
E_O_T;

    launchDialog($dialog);
    $params = array(
          'token'  => $token
        , 'method' => 'auth.getSession'
    );

    $x = callLastFM($params);
    if (empty($x->session->key)) return false;

    $session_key = (string)$x->session->key;
    $fp          = fopen($session_filename, 'w');
    fwrite($fp, "session_key = $session_key\n");
    fclose($fp);
    return $session_key;
}

