#!/bin/bash

interface=org.gnome.Rhythmbox.Player
member=playingUriChanged

# listen for playingUriChanged DBus events,
# each time we enter the loop, we just got an event
# so handle the event, e.g. by printing the artist and title
# see rhythmbox-client --print-playing-format for more output options

dbus-monitor --profile "interface='$interface',member='$member'" |
while read -r line; do
    php tag-by-sa.php
done

