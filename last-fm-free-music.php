<?php

/*

Copyright 2010 Robin Millette <robin@millette.info>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// URL des chansons gratuites du genre Intelligent Dance Music (IDM)
// trouvées sur Last.fm

if (!isset($argv[1])) {
    die("First and only argument must be a musical genre.\n\n");
}

$genre = $argv[1];
$base_url = 'http://www.last.fm/music/+free-music-downloads/' . urlencode($genre);

// initialiser la source, passer dans Tidy pour en faire un beau xhtml (du xml en fait)
function init($url) {
    $tidy = new Tidy($url, array('output-xhtml' => true, 'numeric-entities' => true), 'utf8');
    $tidy->cleanRepair();

    // passe passe pour rendre SimpleXML heureux:
    return new SimpleXMLElement(str_replace('xmlns=', 'ns=', $tidy));
}

// trouve les élements ayant un class donné
function classXpath($xml, $element, $class) {
    // trouver tous les tags <$element> avec un class=$class
    // sachant que l'attribut class pourrait comporter plus d'un terme comme
    // class='$class other' ou
    // class='something $class' ou
    // class='something $class stuff'
    // contains, concat et normalize-space sont des fonctions xsl standard
    return $xml->xpath("//{$element}[contains(concat(' ', normalize-space(@class), ' '), ' $class ')]");
}

// retourner les tags semblables
function getSimilarTags($xml) {
    if (!($targets = classXpath($xml, 'p', 'similarTags'))) return array();
    $tags = array();
    foreach ($targets[0]->a as $tag) {
        $tags[] = str_replace("\n", ' ', $tag);
    }
    sort($tags);
    return $tags;
}

// retourner l'ensemble des chansons trouvées
// et les download si on ne les a pas déjà
function getSongs($xml, &$n_songs) {
    $total_file_size = 0;
    $targets = classXpath($xml, 'a', 'lfmFreeDownloadButton');
    $songs = array();
    foreach ($targets as $song) {
        $title = (string)$song['title'];
        $href = (string)$song['href'];

        // chercher lfmFreeDownloadButton dans la source pour savoir
        // pourquoi on utilise ce regex
        // grosso modo, on parse l'attribut "title" qui contient de la junk
        // matches[1] et matches[2] mappent les parenthèses du regex
        preg_match('/Download free MP3: (.+) - (.+) \(\d+\.\d+ MB\)/', $title, $matches);
        if (('' == $matches[1]) || ('' == $matches[2])) continue;
        $songs[$href] = array('artist' => $matches[1], 'title' => $matches[2]);
        $fn = str_replace(array('/', '\\'), '-', $matches[1]) . '/' . str_replace(array('/', '\\'), '-', $matches[2]) . '.mp3';
        echo "#$n_songs. '{$matches[2]}' by '{$matches[1]}': ";
        if (file_exists($fn)) {
            echo "Already have it.\n";
            continue;
        }
        echo "Downloading... ";
        @mkdir($matches[1]);
        if (@copy($href, $fn)) {
            $total_file_size += @filesize($fn);
            echo "Got it.\n";
            $n_songs++;
        } else echo "Not found.\n";
    }

    // on retourne la taille totale et le tableau des chansons trouvées
    return array($total_file_size, $songs);
//    return array($n_songs, $total_file_size, $songs);
}

// trouver la prochaine page si elle existe
function getNextPage($xml) {
    // un 2e xpath
    // ici on y va brutalement en compant sur le fait que class="nextlink"
    // et ne contiendra pas deux mots ou plus
    // en fait, on devrait faire comme la première fois
    list($next_page_url) = $xml->xpath('//a[@class="nextlink"]');

    // on ne trouve pas? On quitte tout simplement
    if (is_null($next_page_url)) return false;

    $href = (string)$next_page_url['href'];
    preg_match('/\?page=(\d+)/', $href, $matches);
    // on retourne le # de page
    return (int)$matches[1];
}

// on commence à la page 1 (et oui!)
$page = 1;
$total_file_size = 0;
$n_songs = 1;
do {
    echo "Page $page\n";
    // initialisation: cleanup tidy et on retourne un xml (simplexml)
    $xml = init("$base_url?page=$page");
    // on parse toutes les chansons et
    // on les download si on ne les a pas déjà
    list($file_size) = getSongs($xml, $n_songs);
    $total_file_size += $file_size;
// on boucle jusqu'à ce qu'il n'y ait plus de page suivante
} while ($page = getNextPage($xml));
$n_songs--;
$mib = (int)($total_file_size / 1024 / 1024);
echo "Downloaded $n_songs songs in $mib MiB\n";

$similar_tags = join(getSimilarTags($xml), ', ');
if ('' !== $similar_tags) echo "Tags Similar to $genre include $similar_tags\n";

