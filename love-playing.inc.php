<?php

ini_set('user_agent', 'Loverbot');

// last-fm.creds.php defines $user, $api_key and $secret_key
// according to http://www.last.fm/api/account
require 'last-fm.creds.php';

$api_root = 'http://ws.audioscrobbler.com/2.0/';

function callLastFM($params, $post = false) {
    global $api_root;
    global $api_key;
    global $secret_key;

    $params['api_key'] = $api_key;
    ksort($params);
    $p = $str = '';
    foreach ($params as $k=>$v) $str .= "$k$v";
    $str              .= $secret_key;
    $sig               = md5($str);
    $params['api_sig'] = $sig;
    ksort($params);
    foreach ($params as $k=>$v) $p .= "$k=$v&";
    $p = rtrim($p, '&');
    if ($post) {
        $opts = array('http' => array(
              'method'  => 'POST'
            , 'header'  => 'Content-type: application/x-www-form-urlencoded'
            , 'content' => $p
            )
        );
        $context = stream_context_create($opts);
        if ($get = @file_get_contents($api_root, false, $context)) return new SimpleXMLElement($get);
        return false;
    } else {
        $fc = @file_get_contents("$api_root?$p");
        if (!$fc) return false;
        return new SimpleXMLElement($fc);
    }
}

function getNowPlaying($user, $extended) {
    $params = array(
          'user'   => $user
        , 'limit'  => 1
        , 'method' => 'user.getRecentTracks'
    );
    $x     = callLastFM($params);
    $track = $x->recenttracks->track;

    if ($extended && ($track_info = getTrackInfo($user, (string)$track->artist, (string)$track->name))) {
        $track->more->duration   = $track_info->track->duration/1000;
        $track->more->loved      = (string)$track_info->track->userloved;
        $track->more->play_count = (int)$track_info->track->userplaycount;
        $fd                      = (string)$track_info->track->freedownload;
        if ($fd) $track->more->freedownload = $fd;
    }
    $track->more->elapsed = time() - (int)$track->date['uts'];
    return $track; 
}

function getTrackInfo($user, $artist, $title) {
    $params = array(
          'username' => $user
        , 'method'   => 'track.getInfo'
        , 'artist'   => $artist
        , 'track'    => $title
    );
    return callLastFM($params);
}

function loveTrack($artist, $title, $sk) {
    $params = array(
          'method' => 'track.love'
        , 'artist' => $artist
        , 'track'  => $title
        , 'sk'     => $sk
    );
    return callLastFM($params, true);
}

