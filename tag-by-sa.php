<?php

require 'love-playing.inc.php';
echo 'Waiting 30 second... ';
sleep(30);
$playing = getNowPlaying($user, false);
$session_filename = 'lastfm-love.session';
$ar = parse_ini_file($session_filename);
if (isset($ar['session_key'])) $sk = $ar['session_key'];
else die("Can't get session key!\n");
echo "Tagging '{$playing->name}' by '{$playing->artist}'...";
$x = tagTrack($playing->artist, $playing->name, $sk);
echo 'ok' == $x['status'] ? 'ok' : 'oups!';
echo "\n";

function tagTrack($artist, $title, $sk, $tags = null) {
  if (is_null($tags)) $tags = 'hex-hit, 2012, cc-by-sa, creative commons, jamendo, free music, free, netlabel, copyleft';
  elseif (is_array($tags)) $tags = join($tags, ', ');
  elseif (!is_string($tags)) return false;

  $params = array(
    'method' => 'track.addTags',
    'artist' => (string)$artist,
    'track'  => (string)$title,
    'tags'  => $tags,
    'sk'     => $sk,
  );
  return callLastFM($params, true);
}

